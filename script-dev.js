// ==UserScript==
// @name         torn-tools
// @namespace    http://tampermonkey.net/
// @version      1.20
// @license MIT
// @description  Torn tools
// @author       You
// @grant        GM_addStyle
// @run-at       document-start
// @require      https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js
// @require      https://code.jquery.com/jquery-3.6.4.min.js
// @match        https://www.torn.com/*
// ==/UserScript==

GM_addStyle(`

`)

var _apiKey = "";
var _userData, _itemData;
var _mobileMode = false;

function getApiKey(){
    _apiKey = storageGet("apiKey");
    if(_apiKey == null || _apiKey == "" || _apiKey == "null") {
        _apiKey = prompt("Please enter your API Key");
        if(_apiKey !== null)
            storageSet("apiKey", _apiKey);
    }
}
function getApiData(){
    return new Promise((resolve) => {
        Promise.all([getUserData(), getItemData()]).then(() => {
            resolve();
        });
    });
}
function getItemData(){
    const cacheTimeout = 60 * 24 // cache de 1 dia
    var cacheTS = storageGet("item-data-ts");
    if(cacheTS && ((parseInt(cacheTS) + cacheTimeout) > getCurrentMinute())){
        var cacheData = storageGet("item-data");
        _itemData = JSON.parse(cacheData);
        //console.log(_itemData[1]);
        return new Promise((resolve) => {
            resolve();
        });
    }

    console.log("[fx] item api call...");
    return new Promise((resolve) => {
        $.getJSON('https://api.torn.com/torn/?selections=items&key='+_apiKey, function(data){
            _itemData = data.items;
            console.log(_itemData);
            storageSet("item-data", JSON.stringify(_itemData));
            storageSet("item-data-ts", getCurrentMinute());
            resolve();
        });
    });
}
function getUserStats(userID){
    console.log("[fx] user-stats api call...");
    return new Promise((resolve) => {
        $.getJSON('https://api.torn.com/user/'+userID+'?selections=basic,profile,personalstats&key='+_apiKey, function(data){
            console.log(data);
            resolve(data);
        });
    });
}
function getItemMarket(itemID){
    console.log("[fx] item-market api call...");
    return new Promise((resolve) => {
        $.getJSON('https://api.torn.com/market/'+itemID+'?selections=bazaar,itemmarket&key='+_apiKey, function(data){
            console.log(data);
            resolve(data);
        });
    });
}
function getUserData(){
    console.log("[fx] user api call...");
    return new Promise((resolve) => {
        $.getJSON('https://api.torn.com/user/?selections=basic,bars,money,icons,cooldowns,jobpoints,hof,inventory,bazaar,stocks,gym,travel,personalstats,profile,networth,workstats,crimes,perks,attacks,education&key='+_apiKey, function(data){
            _userData = data;
            console.log(_userData);
            resolve();
        });
    });
}
function getStocks(){
    console.log("[fx] stocks api call...");
    return new Promise((resolve) => {
        $.getJSON('https://api.torn.com/torn/?selections=stocks&key='+_apiKey, function(data){
            console.log(data);
            resolve(data);
        });
    });
}


function pageLoaded(){
    return new Promise((resolve) => {
        var iLoad = setInterval(function(){
            if($("#sidebarroot").find("div[class^='content']").find("ul[class^='status-icons']").length > 0){
                _mobileMode = $("div[class^='bars-mobile_']").length > 0;

                $("div[role=main]").append(`
                      <div id="fxPanelMain" class="info-msg-cont blue border-round" style="margin-top:15px; display:none">
                        <div class="info-msg border-round">
                          <i class="info-icon"></i>
                          <div class="delimiter">
                          <div id="fxPanel" class="msg right-round" tabindex="0"><img src="https://www.torn.com/images/v2/main/ajax-loader.gif"></div>
                          </div>
                        </div>
                      </div>
                    `);

                getApiData().then(resolve);
                clearInterval(iLoad);
            }
        }, 100);
    });
}

function fxPanel(html) {
    $("#fxPanelMain").show();
    $("#fxPanel").html(html);
}

$(document).ready(function(){
    'use strict';
    getApiKey();
    pageLoaded().then(function() {
        showTimerList();
        easterEggFind();
        $("#barEnergy").click(function(){window.open('gym.php','_self')});
        $("#barNerve").click(function(){window.open('crimes.php','_self')});
        $("#barHappy").click(function(){window.open('properties.php?step=rentalmarket#','_self')});
        $("#barLife").click(function(){window.open('item.php','_self')});

        if(window.location.pathname == '/imarket.php'){
            itemMarket();
        } else if(window.location.pathname == '/pmarket.php'){
            pointsMarket();
        } else if(window.location.href == 'https://www.torn.com/bazaar.php#/add'){
            bazaarAdd();
        } else if(window.location.pathname == '/bazaar.php'){
            itemInfo();
            bazaar();
        } else if(window.location.href == 'https://www.torn.com/shops.php?step=bitsnbobs'){
            itemInfo();
            bitsnbobs();
        } else if(window.location.href == 'https://www.torn.com/shops.php?step=docks'){
            itemInfo();
            docks();
        } else if(window.location.href == 'https://www.torn.com/shops.php?step=pawnshop'){
            pawnshop();
        } else if(window.location.pathname == '/profiles.php'){
            profiles();
        } else if(window.location.href == 'https://www.torn.com/loader.php?sid=highlow'){
            highlow();
        } else if(window.location.href.includes('https://www.torn.com/trade.php#step=addmoney')){
            tradeAddMoney();
        } else if(window.location.href.includes('https://www.torn.com/loader.php?sid=keno')){
            keno();
        } else if(window.location.href.includes('https://www.torn.com/page.php?sid=stocks')){
            stocks();
        } else if(window.location.href == 'https://www.torn.com/index.php'){
            travel();
        } else if(window.location.pathname == '/travelagency.php'){
            travelagency();
        } else if(window.location.pathname == '/crimes.php'){
            crimes();
        } else if(window.location.pathname == '/city.php'){
            city();
        } else if(window.location.pathname == '/blacklist.php' || window.location.pathname == '/friendlist.php'){
            blacklist();
        } else if(window.location.pathname == '/item.php'){
            itemInfo();
            itemListInfo();
        } else if(window.location.pathname == '/museum.php'){
            itemInfo();
        } else if(window.location.pathname == '/displaycase.php'){
            itemInfo();
            displaycase();
        } else if(window.location.pathname == '/gym.php'){
            gym();
        } else if(window.location.href.includes('https://www.torn.com/page.php?sid=UserList')){
            userList();
        }
    });
});

function userList(){
    var iLoad = setInterval(function(){
        var objList = $("li[class^='user']");
        for(var i = 0; i < objList.length; i++) {
            var curObj = objList.eq(i);
            if(curObj.attr('fx-loaded')=='true') continue;
            var userId = curObj.attr('class').replace('user','');
            console.log(userId);

            getUserStats(userId).then((data)=>{
                curObj.find(".user.faction").replaceWith('');
                curObj.find(".user.name").replaceWith(`<a class="user faction" href="https://www.torn.com/profiles.php?XID=${userId}">${userId}|DT:${data.competition.score}|BD:${data.personalstats.bestdamage}</a>`);
                if(data.competition.score > 0 && data.personalstats.bestdamage < _userData.personalstats.bestdamage){
                    clearInterval(iLoad);
                }
            });

            curObj.attr('fx-loaded', 'true');
            break;
        }
    }, 1000);
}

function displaycase(){
    objActive("div.item-hover[itemid]").then((obj)=>{
        var html = `
        <div style="margin-bottom:10px;">
            <button id="fxRefresh" type="button">[Refresh Prices]</button>
        </div>
        `;
        obj.each((seq) => {
            var itemObj = obj.eq(seq);
            var itemID = itemObj.attr('itemid');
            var itemData = _itemData[itemID];
            var itemUrl = 'https://www.torn.com/imarket.php#/p=shop&type='+itemID;
            var itemImg = `https://www.torn.com/images/items/${itemID}/small.png`
            var valueMkt = formatNumber(itemData.market_value);
            var itemInventory = findInventoryItem(itemID);
            var itemQt = itemInventory?itemInventory.quantity:0;
            html += `
            <p>
               <img src="${itemImg}" style="height:20px; width:20px; object-fit:cover;">
               <span style="display:inline-block; width:150px"><a href="${itemUrl}">${itemData.name}(${itemQt})</a></span>
               <span id="fxitemvalue-${itemID}"><img src="https://www.torn.com/images/v2/main/ajax-loader.gif"></span>
            </p>
            `;
        });
        fxPanel(html);

        var fRefreshPrices = function(){
            obj.each((seq) => {
                var itemObj = obj.eq(seq);
                var itemID = itemObj.attr('itemid');
                var itemData = _itemData[itemID];

                getItemMarket(itemID).then((market)=>{
                    var pctB = 1-(itemData.market_value/market.bazaar[0].cost);
                    var pctI = 1-(itemData.market_value/market.itemmarket[0].cost);
                    var pct = pctB>pctI?pctI:pctB;
                    var color = pct>0?'green':'red';
                    $("#fxitemvalue-"+itemID).html('$' + formatNumber(market.bazaar[0].cost) + ' (<font color="'+color+'">'+formatPercent(pct)+'</font>)');
                });
            });
        };
        fRefreshPrices();
        $("#fxRefresh").click(fRefreshPrices);
    });
}

function stocks(){
    getStocks().then((data)=>{
        var myArray = new Array();
        var result = 0;
        Object.keys(data.stocks).forEach(idStock => {
            var stock = data.stocks[idStock];
            var myStock = _userData.stocks[idStock];
            if(myStock){
                var myQuantity = 0, myTotal = 0;
                Object.keys(myStock.transactions).forEach(idTran => {
					myQuantity += myStock.transactions[idTran].shares;
					myTotal += (myStock.transactions[idTran].bought_price * myStock.transactions[idTran].shares);
				});
                if(myQuantity > 0){
                    var buyPrice = (myTotal / myQuantity).toFixed(2);
                    var profit = (myQuantity * stock.current_price).toFixed(2) - myTotal;

                    myStock.total_value = myTotal;
                    myStock.pm = buyPrice;
                    myStock.profit = profit;
                    myStock.profit_n = profit<0?-profit:profit; // tira o sinal para ordenar

                    myArray.push(myStock);
                    result += profit;
                }
            }
        });
        log(" ");
        log("----- Stocks ($"+formatNumberA(result)+") -----");

        myArray = jsonSort.desc(myArray, "profit_n");

        for(var i = 0; i < myArray.length; i++) {
            var myStock = myArray[i];
            var stock = data.stocks[myStock.stock_id];
            if(myStock.profit_n > 1000000){
                log("> " + stock.acronym + " | $" + formatNumber2(myStock.pm) + " ($"+formatNumberA(myStock.profit)+")");
            }
        }
    });
}

function easterEggFind(){
    if($("#easterEggHuntData").length > 0){
        var eggData = $("#easterEggHuntData").val();
        Logger.clear();
        log("------------------------");
        log(">> Easter Egg Found!! <<");
        log("------------------------");
        eggData = JSON.parse($("#easterEggHuntData").val());
        log("> quantity: " + Object.keys(eggData).length);
        eggData = eggData[0];
        var itemData = _itemData[eggData.eggID];
        console.log(eggData);
        console.log(itemData);
        log("> name: " + itemData.name);
        log("> effect: " + itemData.effect);
        log("> circulation: " + itemData.circulation);
        log("> coordinates: " + eggData.coordinates);
        Logger.open();
    }
}

function gym(){
    log(" ");
    log("----- Gym Perks -----");
    _userData.faction_perks.forEach(perk => {
        if(perk.includes("gym")){
            log(perk);
        }
    });
}

function itemInfo(){
    var itemID, lastItemID;
    var iLoad = setInterval(function(){
        var itemClicado = $("div.item-cont span.item-plate:visible img.torn-item");
        if(itemClicado.length > 0){
            itemID = itemClicado.attr('src').split("/")[3]; // /images/items/554/large.png
            if(lastItemID == undefined || itemID !== lastItemID){
                var itemData = _itemData[itemID];
                var itemUrl = 'https://www.torn.com/imarket.php#/p=shop&type='+itemID;
                console.log(itemData);

                // show prices? <span class="info-msg torn-divider divider-vertical" style="vertical-align:top" id="armory-info-258-">
                getItemMarket(itemID).then((market)=>{
                    var info = itemData.effect==''?itemData.description:itemData.effect

                    if(market.bazaar == undefined || market.bazaar == null || market.bazaar.length == 0) return;

                    var pctB = ''; if(market.bazaar[0]) pctB = formatPercent(1-(itemData.market_value/market.bazaar[0].cost));
                    var pctM = ''; if(market.itemmarket[0]) pctM = formatPercent(1-(itemData.market_value/market.itemmarket[0].cost));

                    var itemPrice = `
                        <span style="display:inline-block; width:120px"><strong>Bazaar</strong> (${pctB})</span><span><strong>Market</strong> (${pctM})</span><br>
                    `;
                    for(var i = 0; i < 5; i++) {
                        var valB = ''; if(market.bazaar[i]) valB = market.bazaar[i].cost>100000000?formatNumberA(market.bazaar[i].cost):formatNumber(market.bazaar[i].cost);
                        var qttB = ''; if(market.bazaar[i]) qttB = market.bazaar[i].quantity;
                        var valM = ''; if(market.itemmarket[i]) valM =market.itemmarket[i].cost>100000000?formatNumberA(market.itemmarket[i].cost):formatNumber(market.itemmarket[i].cost);
                        itemPrice += `<span style="display:inline-block; width:120px">$${valB} <font color="blue">x${qttB}</font></span><span>$${valM}</span><br>`;
                    }
                    var spanInfo = $("span.info-msg:visible");
                    spanInfo.html(`
                        <span style="color:green">${info}</span>
                        <br><br>
                        ${itemPrice}
                        <br><a href="${itemUrl}">Go to Market on Item #${itemID}</a>
                    `);
                    $("span.img-wrap").hide();
                });

                lastItemID = itemID;
            }
            //clearInterval(iLoad);
        }
    }, 500);
}

function itemListInfo(){
    var iLoad = setInterval(function(){
        var lista = $("li[data-item][data-qty]:visible");
        var total = 0;

        if(lista.length > 0){
            for(var i = 0; i < lista.length; i++) {
                var itemID = lista.eq(i).attr('data-item');
                var itemData = _itemData[itemID];
                var itemInventory = findInventoryItem(itemID);
                var spanName = lista.eq(i).find("span.name-wrap");
                if(lista.eq(i).attr('fx-loaded') == undefined){
                    spanName.html(spanName.html() + ' <font color="blue">($'+formatNumberA(itemData.market_value * itemInventory.quantity)+')</font>');
                    lista.eq(i).attr('fx-loaded','true');
                }
                total += itemData.market_value * itemInventory.quantity;
            }

            var spanHeader= $("span.items-name");
            spanHeader.html('$'+formatNumberA(total));
            //clearInterval(iLoad);
        }
    }, 500);
}

function city(){
    objActive("img.map-user-item-icon").then(()=>{
        var itemFound = $("img.map-user-item-icon"); //<img src="https://www.torn.com/images/items/281/small.png" class="leaflet-marker-icon map-user-item-icon leaflet-zoom-hide leaflet-clickable" tabindex="0" style="transform: translate3d(955px, 93px, 0px); z-index: 93; display: block;">
        if(itemFound.length > 0){
            Logger.clear();
            log('----- In City -----');
            var itemValue = 0;
            for(var i = 0; i < itemFound.length; i++) {
                var itemID = itemFound.eq(i).attr("src").split("/")[5]; // https://www.torn.com/images/items/281/small.png
                var itemData = _itemData[itemID];
                log('> '+ itemData.name + ' | $' + formatNumber(itemData.market_value));
                itemValue += itemData.market_value;
            }
            if(itemValue > 0){
                log('TOTAL: $'+ formatNumber(itemValue));
                Logger.open();
            }
        }
    });
}

function blacklist(){
    var iLoad = setInterval(function(){
        for(var i = 0; i < $("li[data-id]").length; i++) {
            var curLi = $("li[data-id]").eq(i);
            if(curLi.attr('fx-loaded')=='true') continue;
            var userID = curLi.find(".name").attr('href').replace(/\D/g,'');
            //curLi.find("div.delete").hide();

            // coloca resultado do ultimo atk
            var kAt = Object.keys(_userData.attacks);
            for (let iAt = kAt.length-1; iAt >= 0; iAt--) {
                var a = _userData.attacks[kAt[iAt]];
                var d = formatDate(new Date(a.timestamp_ended*1000))
                var respect = formatNumber1(a.respect_gain);

                if(userID == a.defender_id){
                    console.log(a);
                    var color = a.stealthed==1?'gray':'blue';
                    curLi.find(".user.faction").replaceWith(`<a class="user faction" style="color:${color}" href="https://www.torn.com/loader.php?sid=attackLog&ID=${a.code}" title="${a.result}">${respect}</a>`);
                    break;
                }
            }

            if(curLi.find("div.status").html().includes('red')){ // <span class="user-red-status "> Hospital </span>
                curLi.find(".user.name").css('color','red');
            }

            curLi.attr('fx-loaded', 'true');
        }
    }, 500);

    // lista
    log(' ');
    log('----- Attack List -----');
    var kAt = Object.keys(_userData.attacks);
    for (let iAt = kAt.length-1; iAt >= 0; iAt--) {
        var a = _userData.attacks[kAt[iAt]];
        var d = formatDate(new Date(a.timestamp_ended*1000))
        var r = formatNumber1(a.respect_gain);
        log(`> ${d} ${a.attacker_name}[${a.attacker_id}] => ${a.defender_name}[${a.defender_id}] - ${a.result}: ${r}`);
    }
}

function crimes(){
    logJson('Criminal Record', _userData.criminalrecord);
}

function travelagency(){
    var travelList = {
        "mex":{"personalstats":"mextravel", "name":"Mexico",        "price":6500,  "standard":26,  "airstrip":18,  "wlt":13,  "bct":8,  "itens":[258,260]},
        "cay":{"personalstats":"caytravel", "name":"Cayman",        "price":10000, "standard":35,  "airstrip":25,  "wlt":18,  "bct":11, "itens":[617,618]},
        "can":{"personalstats":"cantravel", "name":"Canada",        "price":9000,  "standard":41,  "airstrip":29,  "wlt":20,  "bct":12, "itens":[261,263]},
        "haw":{"personalstats":"hawtravel", "name":"Hawaii",        "price":11000, "standard":134, "airstrip":94,  "wlt":67,  "bct":40, "itens":[264]},
        "uni":{"personalstats":"lontravel", "name":"UK",            "price":18000, "standard":159, "airstrip":111, "wlt":80,  "bct":48, "itens":[266,267,268]},
        "arg":{"personalstats":"argtravel", "name":"Argentina",     "price":21000, "standard":167, "airstrip":117, "wlt":83,  "bct":50, "itens":[269,271]},
        "swi":{"personalstats":"switravel", "name":"Switzerland",   "price":27000, "standard":175, "airstrip":123, "wlt":88,  "bct":53, "itens":[272,273]},
        "jap":{"personalstats":"japtravel", "name":"Japan",         "price":32000, "standard":225, "airstrip":158, "wlt":113, "bct":68, "itens":[277]},
        "chi":{"personalstats":"chitravel", "name":"China",         "price":35000, "standard":242, "airstrip":169, "wlt":121, "bct":72, "itens":[274,276]},
        "uae":{"personalstats":"dubtravel", "name":"UAE",           "price":32000, "standard":271, "airstrip":190, "wlt":135, "bct":81, "itens":[384,385]},
        "sou":{"personalstats":"soutravel", "name":"South Africa",  "price":40000, "standard":297, "airstrip":208, "wlt":149, "bct":89, "itens":[281,282]}
    }

    var travelHtml = "";
    var travelCapacity = getUserCarryItems(); // {"normal":5,"suitcase":4,"airstrip":10,"faction":10,"job":0,"wlt":0,"book":0,"event":0,"mode":"airstrip","total":29}
    var travelType = travelCapacity.mode;

    Object.entries(travelList).forEach((entry) => {
        const [key, value] = entry;
        const tempo = value[travelType] * 60;
        value.horaIda = getHoraReal(tempo);
        value.horaVolta = getHoraReal(tempo * 2);
        var itemHtml = '';
        for(var i = 0; i < value.itens.length; i++) {
            var itemID = value.itens[i];
            var itemData = _itemData[itemID];
            var itemInventory = findInventoryItem(itemID);
            var itemQt = itemInventory?itemInventory.quantity:0;
            if(_mobileMode){
            itemHtml += `
            <a href="https://www.torn.com/imarket.php#/p=shop&type=${itemID}">
                 <img src="https://www.torn.com/images/items/${itemID}/small.png" title="${itemData.name} (Owned: ${itemQt})" style="height:20px; width:20px; object-fit:cover;">
            </a>`
            } else {
            itemHtml += `
            <a href="https://www.torn.com/imarket.php#/p=shop&type=${itemID}">
                 <img src="https://www.torn.com/images/items/${itemID}/small.png" title="${itemData.name} (Owned: ${itemQt})" style="height:20px; width:20px; object-fit:cover;">${itemQt}
            </a>`
            }
        }
        travelHtml += `
        <p>
           <span style="display:inline-block; width:40px"><strong>${key}</strong>:</span>
           <span style="">${value.horaIda} &#8660; ${value.horaVolta} &nbsp${itemHtml}</span>
        </p>
        `;
    });
    //div.travel-agency
    fxPanel(travelHtml);
    $("h4#skip-to-content").html('Travel Agency: <font color="green">'+travelType+' ('+travelCapacity.total+')</font>');

    logJson('Travel Capacity', getUserCarryItems());
}

function showTimerList() {
    var timerList = new Array();
    timerList.push({name:"Bank", timeleft:_userData.city_bank.time_left, emoji:"&#128178;"});
    if(_userData.energy.fulltime > 0) timerList.push({name:"Energy", timeleft:_userData.energy.fulltime, url:"https://www.torn.com/gym.php", emoji:"&#9889;"});
    if(_userData.nerve.fulltime > 0) timerList.push({name:"Nerve", timeleft:_userData.nerve.fulltime, url:"https://www.torn.com/crimes.php#/step=main", emoji:"&#127775;"});
    if(_userData.happy.fulltime > 0) timerList.push({name:"Happy", timeleft:_userData.happy.fulltime, emoji:"&#128578;"});
    if(_userData.life.fulltime > 0) timerList.push({name:"Life", timeleft:_userData.life.fulltime, emoji:"&#128151;"});
    if(_userData.cooldowns.booster > 0) timerList.push({name:"Booster CD", timeleft:_userData.cooldowns.booster, emoji:"&#127870;"});
    if(_userData.cooldowns.drug > 0) timerList.push({name:"Drug CD", timeleft:_userData.cooldowns.drug, emoji:"&#128138;"});
    if(_userData.cooldowns.medical > 0) timerList.push({name:"Medical CD", timeleft:_userData.cooldowns.medical, emoji:"&#128137;"});
    if(_userData.education_timeleft > 0) timerList.push({name:"Education", timeleft:_userData.education_timeleft, url:"https://www.torn.com/education.php", emoji:"&#127891;"});
    if(_userData.status.until > 0){ // hospital, etc
        timerList.push({name:_userData.status.state, timeleft:_userData.status.until-_userData.server_time, emoji:"&#128128;"});
    }
    if(_userData.travel && _userData.travel.time_left > 0){
        timerList.push({name:'Travel to '+_userData.travel.destination, timeleft:_userData.travel.time_left, emoji:"&#9992;"});
    }
    if(_userData.icons.icon85 && _userData.icons.icon85.includes('0 days')){ // OC - "Organized Crime - Hijack a plane - 6 days, 22 hours, 19 minutes and 26 seconds"

    }
    if(_userData.chain && _userData.chain.current >= 10 && _userData.chain.timeout > 0){
        timerList.push({name:"Chain ("+_userData.chain.current+")", timeleft:_userData.chain.timeout, url:"https://www.torn.com/factions.php?step=your#/war/chain", emoji:"&#128279;"});
    }
    if(_userData.icons.icon3){
        var donatorDays = _userData.icons.icon3?parseInt(_userData.icons.icon3.split(' - ')[1].replace(' days','')):0;
        if(donatorDays > 0){
            timerList.push({name:"Donator", timeleft:donatorDays*24*60*60, url:'https://www.torn.com/donator.php?code=USD', emoji:"&#11088;"});
        }
    }

    timerList = jsonSort.asc(timerList, "timeleft"); // ordena pelo mais recente

    log("----- Timer List -----");
    for(var i = 0; i < timerList.length; i++) {
        var timerItem = timerList[i];
        var tempoRestante = timerItem.timeleft; // em s
        tempoRestante = parseInt(tempoRestante/60); // em min
        if(tempoRestante > 60 * 24) tempoRestante = '+'+parseInt(tempoRestante/(60*24))+'d'; // dias
        else if(tempoRestante > 60) tempoRestante = '+'+parseInt(tempoRestante/60)+'h'; // horas
        else tempoRestante = tempoRestante+'m'; // min

        log("> " + getHoraReal(timerItem.timeleft) + " - " + timerItem.name + " ("+tempoRestante+")");

        if(i == 0){
            $("#loggerTab").html((timerItem.emoji?timerItem.emoji+' ':'')+tempoRestante);
        }
	}
}

function travel(){
    var iRows = setInterval(function(){
        // remove animacao
        if($("div.stage").is(":visible")){
            $("div.stage").find("div.clouds").hide();
            $("div.stage").find("img").hide();
            $("div.stage").css("height","200px");
        }

        // flight info
        var divInfo = $("div.flight-info");
        if(divInfo.is(":visible") && divInfo.find("#fx-flight-info").length==0){
            var chegada = getHoraReal(_userData.travel.time_left);
            divInfo.append(`<div id="fx-flight-info" align="center" style="width:100%; padding:5px; color:blue">Arrive at ${chegada}</div>`);
        }

        // buy list
        var divMain = $("div.travel-agency-market");
        if(divMain.is(":visible")){
            divMain.find("ul.users-list li:contains('Other')").hide();
            divMain.find("ul.users-list li:contains('Primary')").hide();
            divMain.find("ul.users-list li:contains('Secondary')").hide();
            divMain.find("ul.users-list li:contains('Melee')").hide();
            divMain.find("ul.users-list li:contains('Defensive')").hide();
            divMain.find("ul.users-list li:contains('Clothing')").hide();
            divMain.find("ul.users-list li:contains('Temporary')").hide();
            divMain.find("ul.users-list li:contains('Drug'):not(:contains('Xanax'))").hide();
            divMain.find("ul.users-list li:contains('Electronic')").hide();

            var maxCarry = $("div.user-info > div > div > div > span:nth-child(4)").html();
            var inputAmount = divMain.find("input.numb[name='amount']:visible");

            for(var iA = 0; iA < inputAmount.length; iA++){
                var inQtt = inputAmount.eq(iA);
                if(inQtt.attr('data-fx-loaded')=='true') continue;
                inQtt.attr('data-fx-loaded','true');
                reactInputHack(inQtt, maxCarry);
                inQtt.focus();
            }
        }

        if($("#countrTravel").length > 0){
            document.title =  $("#countrTravel").html() + ' to ' + _userData.travel.destination;
        }
    }, 1000);
}

function keno(){
    $(".content-title").after('<div style="margin-top:10px;">'+
                              '<button id="fxKenoRandom5" type="button">[Random 5]</button>'+
                              '<button id="fxKenoRandom6" type="button">[Random 6]</button>'+
                              '<button id="fxKenoRandom7" type="button">[Random 7]</button>'+
                              '<button id="fxKenoRandom8" type="button">[Random 8]</button>'+
                              '<button id="fxKenoRandom9" type="button">[Random 9]</button>'+
                              '<button id="fxKenoRandom10" type="button">[Random 10]</button>'+
                              '</div>');
    var fRandom = function(qt){
        $("#clearBtn").click();
        var selecionados = [];
        while(qt > 0){
            var r = parseInt((Math.random()*80)+1);
            if(!selecionados.includes(r)){
                $("span#keno_item_"+r).click();
                qt--;
                selecionados.push(r);
            }
        }
    }
    $("#fxKenoRandom5").click(function(){fRandom(5);});
    $("#fxKenoRandom6").click(function(){fRandom(6);});
    $("#fxKenoRandom7").click(function(){fRandom(7);});
    $("#fxKenoRandom8").click(function(){fRandom(8);});
    $("#fxKenoRandom9").click(function(){fRandom(9);});
    $("#fxKenoRandom10").click(function(){fRandom(10);});
}

function tradeAddMoney(){
    setTimeout(function(){
        $(".input-money-group").after('&nbsp;&nbsp;&nbsp;&nbsp;<input id="fxMinOn" type="checkbox" value="S"><input id="fxMin" size="12" type="text" value="10.000.000"> <font color=red>$ to auto add</font>');
        $("#fxMin").keydown(function(){$("#fxMinOn").prop('checked','')});
    }, 1000);

    var iLoop = setInterval(function(){
        if(!$("#fxMinOn").is(":checked")) return;

        $(".input-money-symbol").click(); // add all

        var money = $("input[type='text'].input-money").val().replace(/\D/g,''); // all money
        var minTrigger = $("#fxMin").val().replace(/\D/g,'');

        if(minTrigger > 0 && parseInt(money) >= parseInt(minTrigger)){
            if($("input.torn-btn").is(":enabled")){
                $("input.torn-btn").click();
            }
        }
    }, 200);
}

function bazaarAdd(){

}

function highlow(){
    var myDivMain = $("div.content-wrapper")
    var myDivControl = myDivMain.append('<div style="margin-top:30px;"><select id="fxAutoPlayHL"><option value="N">Auto Play - OFF</option><option value="S">Auto Play - ON</option></select></div>');
    var myDivInfo = myDivMain.append('<div style="margin-top:30px;">');

    myDivInfo.append('Script running...');

    var escolha = '';
    var cpuCard = '';
    var youCard = '';
    var val_cpuCard = 0;
    var val_youCard = 0;

    var iLoop = setInterval(function(){
        var btnStart = $("span.btn-title:contains('START')");
        var btnContinue = $("div.continue");
        var btnLower = $("span:contains('Lower')");
        var btnHigher = $("span:contains('Higher')");
        var tokens = $(".tokens").html();
        var autoOff = $("#fxAutoPlayHL").val()=='N';

        if(autoOff){return;}

        if(btnStart.is(':visible') || tokens == 0){
            myDivInfo.append(' | <strong>tokens: '+tokens+'</strong>');
            btnStart.click();
        }

        cpuCard = $(".dealer-card").find(".rating").html();
        youCard = $(".you-card").find(".rating").html();

        val_cpuCard = cpuCard;
        if(val_cpuCard == 'J') val_cpuCard = 11;
        if(val_cpuCard == 'Q') val_cpuCard = 12;
        if(val_cpuCard == 'K') val_cpuCard = 13;
        if(val_cpuCard == 'A') val_cpuCard = 14;

        // escolha
        if(btnLower.is(':visible') && btnHigher.is(':visible')){
            if(val_cpuCard >= 8){ // 2,3,4,5,6,7  ,8,  9,10,11,12,13,14
                btnLower.click();
                escolha = 'lower';
            } else {
                btnHigher.click();
                escolha = 'higher';
            }
        }

        // auto-continue
        if(btnContinue.is(':visible')){
            val_youCard = youCard;
            if(val_youCard == 'J') val_youCard = 11;
            if(val_youCard == 'Q') val_youCard = 12;
            if(val_youCard == 'K') val_youCard = 13;
            if(val_youCard == 'A') val_youCard = 14;

            if(youCard && youCard != ''){
                myDivInfo.append(' | ' + cpuCard + '/' + youCard + ' ' + $(".win-info").html());
            }
            btnContinue.click();
        }
    }, 1500);
}

function pawnshop(){ // https://www.torn.com/profiles.php?XID=2637524
    var objSpan = $('<font color=blue>').appendTo("form[action='pointsell.php?step=points']");
    var inputQtt = $(".input-money[type=text]");
    inputQtt.val(50).change(function(){
        var qtt = $(this).val().replace(',','');
        objSpan.html('$' + formatNumber(qtt * 45000));
    }).trigger('change');

    // total points
    var points = $("#pointsPoints").parent().find("span.value___2EvPm").html();

    // show some sell values
    if(points > 0){
        $("#skip-to-content").append('<font color=blue>($' + formatNumber(points * 45000) + ')</font>')

        $("div.sell-points").append('<br>');
        $("div.sell-points").append('<hr>');
        $("div.sell-points").append('<br>');

        var somas = '<font color=red>';
        for(var p = 50; p < points; p = p + 50){
            somas += 'x'+(p)+' = $'+(formatNumber(p * 45000))+'<br>';
        }
        somas += '</font>';
        $("div.sell-points").append(somas);
    }
}

function profiles(){
    var lastStatus = '';
    var userID = url.param('XID');

    var iLoop = setInterval(function(){
        var status = $(".main-desc").html().trim();
        status = status.split(' for ')[0]; // In hospital for (remove countdown)
        var userName = $(".user-info-value").find(".bold").html();

        if(lastStatus !== '' && lastStatus !== status){
            notify(userName + " - Status Change: " + lastStatus + ' -> ' + status);
            clearInterval(iLoop);
        }
        lastStatus = status;
        console.log(userName + " - Status: " + status);
    }, 5000);

    $("div.basic-information").after(`
    <div class="fx-profile profile-right-wrapper right" style="margin-bottom:15px">
    <div>
      <div class="title-green top-round">[Fx] Profile</div>
        <div class="cont bottom-round ">
          <div class="profile-container personal-info" style="height: 100%;">
            <ul class="info-table">
              <li><div style="margin-left:15px"><img src="https://www.torn.com/images/v2/main/ajax-loader.gif"></div></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    `);

    var fAddInfo = function(nome, valor, valor2, color){
        color = color?color:'blue';
        $("div.fx-profile ul.info-table").append(`
          <li style="color:${color}">
            <div class="user-information-section"><span class="bold">${nome}</span></div>
            <div class="user-info-value"><span style="display:inline-block; width:100px;">${valor}</span><span>${valor2}</span></div>
          </li>
        `);
    }
    var fAddCompare = function(nome, data, statName){
        fAddInfo(nome, formatNumber(data.personalstats[statName]), formatNumber(_userData.personalstats[statName]));
    }

    //$("div.fx-profile").css('height','100%');
    getUserStats(userID).then((data)=>{
        $("div.fx-profile ul li").remove();

        fAddInfo("STATS", '<strong>'+data.name+'</strong>', '<strong>'+_userData.name+'</strong>');
        fAddInfo("Networth", '$'+formatNumberA(data.personalstats.networth), '$'+formatNumberA(_userData.personalstats.networth));
        fAddCompare("Awards", data, 'awards');
        fAddCompare("ELO", data, 'elo');
        fAddInfo("Activity", formatNumber(data.personalstats.useractivity/60/60/24), formatNumber(_userData.personalstats.useractivity/60/60/24));
        fAddInfo("Avg.Damage", formatNumber(data.personalstats.attackdamage/data.personalstats.attackhits), formatNumber(_userData.personalstats.attackdamage/_userData.personalstats.attackhits));
        fAddCompare("Best Damage", data, 'bestdamage');
        fAddCompare("Xanax Taken", data, 'xantaken');

        // lista ataques
        var kAt = Object.keys(_userData.attacks);
        for (let iAt = kAt.length-1; iAt >= 0; iAt--) {
            var a = _userData.attacks[kAt[iAt]];
            if(userID == a.defender_id){
                console.log(a);
                fAddInfo('&#9876; '+a.result, formatDate(new Date(a.timestamp_ended*1000)), formatNumber2(a.respect_gain) + ' <a href="https://www.torn.com/loader.php?sid=attackLog&ID='+a.code+'">[log]</a>', 'red');
            }
        }

        logJson("Profile: "+data.name, data.personalstats);
    });
}

function docks(){
    npcSellTotal();
}

function npcSellTotal(){
    var mainDiv = $("div.sell-items-wrap");
    var itemList = mainDiv.find("li[data-id]:visible"); // <ul class="sell-items-list">
    var totG = 0;

    for(var a = 0; a < itemList.length; a++){
        var qt = itemList.eq(a).find('input[name="amount"]').attr('data-max');
        var vl = itemList.eq(a).find('li.value').html();
        vl = vl.replace(/\D/g,'');
        var tot = qt * vl;
        totG += tot;

        itemList.eq(a).find("li.desc").append(' <font color=blue>($' + formatNumber(tot) + ')</font>');
    }

    mainDiv.find('ul.sell-act').find("li.sell").append(' <font color=red>(Total: $' + formatNumber(totG) + ')</font>')
}

function bitsnbobs(){
    // hide itens !=  (grenade, heg, throwing knife, fire hidrant, afro comb, Maneki Neko, Yucca Plant)
    var divSell = $("div.sell-items-wrap");
    var iSell = divSell.find("li[data-item]");
    for(var s = 0; s < iSell.length; s++){
        var iID = iSell.eq(s).data('item');
        if(iID == 220 || iID == 242 || iID == 257 || iID == 410 || iID == 406 || iID == 279 || iID == 409) continue;
        iSell.eq(s).hide(); //find('input[name="amount"]').attr('disabled','disabled');
    }
    //<button class="wai-btn">Select All</button>
    divSell.find("li.select").hide();

    // destaca beer
    $("span#180-name").css('color','red');
    $("span#180-price").css('color','red');
    $("span#180-stock").css('color','red');

    var buyClicked = false;
    var iLoop = setInterval(function(){
        var mainDiv = $("div.buy-items-wrap"); // <div class="buy-items-wrap">
        var iAmount = mainDiv.find("input[name='buyAmount[]']"); // <input id="186" type="text" value="1" maxlength="3" name="buyAmount[]" autocomplete="new-amount">
        for(var a = 0; a < iAmount.length; a++){
            if(iAmount.eq(a).val() == 1){
                iAmount.eq(a).val(100);
            }
        }

        // auto buy beer
        if(!buyClicked && $("input#180").parent().find("button:contains('Buy')").length > 0){
            $("input#180").parent().find("button:contains('Buy')").click();
            buyClicked = true;
        }

        var btnYes = mainDiv.find("a:contains('Yes'):visible"); // <a href="#" class="wai-support yes m-right10 bold t-blue h" data-id="97">Yes</a>
        if(btnYes.length == 1){
            btnYes.click();
        }
    }, 400);

    // shot tct
    var objTime = $("div.title-black[role=heading]");
    var objText = objTime.html();
    var iLoopTCT = setInterval(function(){
        var tct = $(".server-date-time").html(); // Fri 14:04:57 - 22/01/21
        if(tct) tct = tct.split(' - ')[0].split(' ')[1];
        objTime.html(objText + " (TCT: "+tct+")");
        // auto buy
        var buyTime = tct.split(':');
        if((buyTime[1]=='00' || buyTime[1]=='15' || buyTime[1]=='30' || buyTime[1]=='45') && (buyTime[2]=='05')){
            window.location.reload();
        }
    }, 1000);

    npcSellTotal();
}

function pointsMarket(){
    var iLoop = setInterval(function(){
        var aYes = $("ul.users-point-sell").find("a:contains('Yes'):visible");

        if(aYes.length == 1){
            aYes.click(); // confirma automaticamente
        }
    }, 100);
}

function itemMarket(){
    var itemID = new URLSearchParams(window.location.href).get('type');

    var iLoopG = setInterval(function(){
        var obj = $('div.msg[role="alert"]');
        if(obj.length > 0){
            var money = $("span[data-money]").attr('data-money').replace(/\D/g,'');
            var vNPC = new URLSearchParams(window.location.href).get('_fxn'); // valor do npc
            var vOfe = new URLSearchParams(window.location.href).get('_fxv'); // valor ofertado
            var qOfe = new URLSearchParams(window.location.href).get('_fxq'); // quantidade ofertada
            var htmlTxt = '<a href="https://www.torn.com/bazaar.php#/add">Bazaar</a>'
            if(vNPC !== null){
                htmlTxt += " | <strong>Sell:</strong> $"+formatNumber(parseInt(vNPC));
            }
            if(vOfe !== null){
                var necessarioSacar = parseInt(vOfe)*parseInt(qOfe) - money;
                if(necessarioSacar > 0){
                    htmlTxt += " | <strong>Need:</strong> <font color='red'>$"+formatNumberA(necessarioSacar)+'</font>';
                }
            }
            obj.html(htmlTxt);
        }
    }, 500);

    // destaque no bazaar
    var iLoopV = setInterval(function(){
        var vParam = new URLSearchParams(window.location.href).get('_fxv'); // valor a destacar
        if(vParam == undefined || vParam==null) return;
        if($("ul.guns-list").is(':visible')){
            var ulBazaar = $("ul.guns-list").find("li");
            for(var j = 0; j < ulBazaar.length; j++){
                if(ulBazaar.eq(j).html()=="") break;
                var vBazaar = ulBazaar.eq(j).find(".price").html().split(" <")[0].replace(/\D/g,''); // $59,895 <span class="stock t-gray-9">(73)</span>
                if(parseInt(vParam) >= parseInt(vBazaar)){
                    ulBazaar.eq(j).find(".price").css('color','red')
                }
            }
            //clearInterval(iLoopV);
        }
    }, 500);

    // destaque no market
    var iLoopM = setInterval(function(){
        var vParam = new URLSearchParams(window.location.href).get('_fxv'); // valor a destacar
        if(vParam == undefined || vParam==null) return;
        if($("ul.items").is(':visible')){
            var ulItems = $("ul.items").find("ul.item");
            for(var i = 0; i < ulItems.length; i++){
                var vMarket = ulItems.eq(i).find("li.cost").html().replace(/\D/g,'');
                if(parseInt(vParam) >= parseInt(vMarket)){
                    ulItems.eq(i).find("li.cost").css('color','red');
                }
            }
            //clearInterval(iLoopM);
        }
    }, 500);

    var iLoop = setInterval(function(){  // confirma automaticamente
        if($("a[data-action='buyItemConfirm']:visible")){
            $("a[data-action='buyItemConfirm']:visible").trigger('click');
        }
    }, 100);
}

function bazaar(){
    var iLoop = setInterval(function(){
        var divBazaar = $("#bazaarRoot");
        var qttInput = divBazaar.find("input[class^='numberInput___']"); // <input type="text" class="numberInput___1-rmF buyAmountInput___1yrv8" aria-label="Amount of Honda Accord" value="1">

        // Yes click
        var btnYes = divBazaar.find("button[aria-label='Yes']");
        if(btnYes.length == 1){
            btnYes.click();
            return;
        }

        var money = $("span[data-money]").attr('data-money').replace(/\D/g,'');

        if(!_mobileMode && qttInput.length == 1){
            if(qttInput.attr('fx-tool')=='true') return;
            var objPrice = divBazaar.find("span[class^='price___']");
            if(objPrice == undefined)objPrice = divBazaar.find("p[class^='price___']");
            var buyButton = $("button[class^='buy___']");
            if(objPrice == undefined || objPrice.html()==undefined) return;
            var price = objPrice.html().replace(/\D/g,'');
            var qttMax = divBazaar.find("span[class^='amount___']").html().replace(/\D/g,'');//<span class="amount___1G62R infoLine___1fPlB">(7 in stock)</span>
            var myMax = parseInt(money/price); // buy all
            if(myMax > qttMax) myMax = qttMax;
            if(myMax == 0){
                reactInputHack(qttInput, 0);
            } else if(qttInput.val()==0){
                reactInputHack(qttInput, 1);
            } else if(qttInput.val()==1){
                reactInputHack(qttInput, myMax);
            }
            if(price == 1 && buyButton.length == 1){
                buyButton.click();
            }
            qttInput.attr('fx-tool','true');
        }

        if(_mobileMode) { // mobile version
            for(var i = 0; i < qttInput.length; i++){
                if(qttInput.eq(i).attr('fx-tool')=='true') continue;
                qttInput.eq(i).attr('fx-tool','true');
                objPrice = $("p[class^='price___']").eq(i);
                price = objPrice.html().replace(/\D/g,'');
                //qttMax = qttInput.eq(i).attr('max');
                qttMax = $("span[class^='amountValue___']").eq(i).html().replace(/\D/g,'');
                myMax = parseInt(money/price); // buy all
                if(myMax > qttMax) myMax = qttMax;
                if(myMax == 0){
                    reactInputHack(qttInput.eq(i), 0);
                } else if(qttInput.eq(i).val()==0){
                    reactInputHack(qttInput.eq(i), 1);
                } else if(qttInput.eq(i).val()==1){
                    reactInputHack(qttInput.eq(i), myMax);
                }
            }
        }
    }, 200);
}

// HACK to simulate input value change
// https://github.com/facebook/react/issues/11488#issuecomment-347775628
function reactInputHack(inputjq, value) {
    // get js object from jquery
    const input = $(inputjq).get(0);

    let lastValue = 0;
    input.value = value;
    let event = new Event('input', { bubbles: true });
    // hack React15
    event.simulated = true;
    // hack React16 ?????descriptor??value,??????
    let tracker = input._valueTracker;
    if (tracker) {
        tracker.setValue(lastValue);
    }
    input.dispatchEvent(event);
}

function findItem(itemId, listName){
    var oItem = undefined;
    if(_userData!==undefined && _userData[listName]!==undefined && _userData[listName]!==null){
        for (var i = 0; i < _userData[listName].length; i++) {
            if(_userData[listName][i].ID == itemId){
                oItem = _userData[listName][i];
                break;
            }
        }
    }
    return oItem;
}
function findInventoryItem(itemId){
    return findItem(itemId, 'inventory');
}
function findBazaarItem(itemId){
    return findItem(itemId, 'bazaar');
}
function findDisplayItem(itemId){
    return findItem(itemId, 'display');
}

// https://wiki.torn.com/wiki/Travel#Carrying_Items
function getUserCarryItems() {
    // Suitcase
    var suitcase = 0;
    if(findInventoryItem(421) !== undefined) {suitcase = 4;}
    else if(findInventoryItem(420) !== undefined) {suitcase = 3;}
    else if(findInventoryItem(419) !== undefined) {suitcase = 2;}

    // PI airstrip
    var airstrip = 0
    if(_userData.property_perks[0] == "+ Access to airstrip"){
        airstrip = 10;
    }

    // faction perks (+ 10 travel item capacity)
    var faction = 0;
    _userData.faction_perks.forEach(perk => {
        if(perk.includes("travel item capacity")){
            faction = parseInt(perk.split(" ")[1]);
            return;
        }
    });

    // The 5 Star Lingerie Store gives Job special adds an extra 2 items
    // The 3 and 10 star Cruise Line Agency Job Specials add an extra 2 and 3 items respectively. I.e. They stack.
    // 7* Flower Shops allow you to carry 5 additional flowers.
    // 7* Toy Shops allow you to carry 5 additional plushies.
    var job = 0;

    // WLT BB
    var wlt = 0;
    if(_userData.stocks !== undefined && _userData.stocks[30] !== undefined && _userData.stocks[30].benefitActive){
        wlt = 10;
    }

    // Book : Smuggling For Beginners, Increases travel items by 10 for 31 days.
    var book = 0;

    // total
    var total = 5 + suitcase + airstrip + faction + job + wlt + book;

    // tourism day on the 27th of September doubles carrying capacity.
    var d = new Date();
    var event = 0;
    if(d.getDate()==27 && d.getMonth()==(9-1)){
        event = total;
    }

    var mode = 'standard';
    if(wlt > 0) mode ='wlt';
    if(airstrip > 0) mode ='airstrip';

    return {
        "normal":5,
        "suitcase":suitcase,
        "airstrip":airstrip,
        "faction":faction,
        "job":job,
        "wlt":wlt,
        "book":book,
        "event":event,
        "mode":mode,
        "total":total
    };
}

function logJson(nome, json){
    log(' ');
    log('----- '+nome+' -----');
    for (var key in json) {
        var value = json[key];
        log('> '+key+': ' +(isNaN(value)?value:formatNumber(value)));
    }
}

// ------------------------------------------

function sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
        currentDate = Date.now();
    } while (currentDate - date < milliseconds);
}

function objActive(objStr){
    return new Promise((resolve) => {
        var iObj = setInterval(()=>{
            if($(objStr) !== undefined && $(objStr).length > 0){
                resolve($(objStr));
                clearInterval(iObj);
            }
        },250);
    });
}

function getCurrentMinute(){
	return parseInt(Date.now()/1000/60);
}

function getHoraReal(time_left){ // em seg
    if(time_left == undefined || time_left <= 0){return '-';}
    var fZeroPadding = function(number, length) {
        return (Array(length).join('0') + number).slice(-length);
    };
    var dNow = new Date();
    var dEvent = new Date(Date.now() + (time_left*1000));
    var dDif = dEvent.getTime() - dNow.getTime();
    dDif = parseInt(dDif / 1000 / 60 / 60 / 24); // 1dia
    if(dDif <= 0){
        return fZeroPadding(dEvent.getHours(),2)+'h'+fZeroPadding(dEvent.getMinutes(),2);
    } else {
        return fZeroPadding(dEvent.getMonth()+1,2)+'/'+fZeroPadding(dEvent.getDate(),2);
    }
}
function removeHtmlTags(str){
    if ((str===null) || (str===''))
        return false;
    else
        str = str.toString();
    return str.replace(/<[^>]*>/g, '');
}

function notify(msg) {
    if (!("Notification" in window)) {
        console.log("This browser does not support desktop notification");
    }
    else if (Notification.permission === "granted") {
        var notification = new Notification(msg);
    }
    else if (Notification.permission !== "denied") {
        Notification.requestPermission().then(function (permission) {
            if (permission === "granted") {
                var notification = new Notification(msg);
            }
        });
    }
}

function formatNumber(n){
    return numeral(parseFloat(n).toFixed(0)).format('0,0');
}
function formatNumber1(n){
    return numeral(parseFloat(n).toFixed(1)).format('0,0.0');
}
function formatNumber2(n){
    return numeral(parseFloat(n).toFixed(2)).format('0,0.00');
}
function formatNumberA(n){
    return numeral(parseFloat(n).toFixed(0)).format('0,0[.]00a');
}
function formatPercent(valor){
    if(valor==undefined) valor = 0;
    return numeral(parseFloat(valor*100).toFixed(1).toString()).format('0,0.0') + "%";
}
function formatDate(d){
	var fmt2 = function(n){return n < 10 ? '0' + n : n;}
	return fmt2(d.getDate()) + '/' + fmt2(d.getMonth()+1) + '/' + d.getFullYear();
}

function randomInt(min,max) {
    return min + Math.floor(((max+1) - min) * Math.random());
}

function storageSet(name, value){
    window.localStorage.setItem('fx-'+name, value);
}
function storageGet(name){
    return window.localStorage.getItem('fx-'+name);
}

var jsonSort = {
    _isFloat: function(n){
		return Number(n) === n && n % 1 !== 0;
	},

	_sortDesc: function(property){
		return function(a,b){
			if(jsonSort._isFloat(a[property]) && jsonSort._isFloat(b[property])){
				if(parseFloat(a[property]) < parseFloat(b[property]))
					return 1;
				else if(parseFloat(a[property]) > parseFloat(b[property]))
					return -1;
			} else {
				if(a[property] < b[property])
					return 1;
				else if(a[property] > b[property])
					return -1;
			}
			return 0;
		}
	},

	_sortAsc: function(property){
		return function(a,b){
			if(jsonSort._isFloat(a[property]) && jsonSort._isFloat(b[property])){
				if(parseFloat(a[property]) > parseFloat(b[property]))
					return 1;
				else if(parseFloat(a[property]) < parseFloat(b[property]))
					return -1;
			} else {
				if(a[property] > b[property])
					return 1;
				else if(a[property] < b[property])
					return -1;
			}
			return 0;
		}
	},

	asc: function(list, property){
		return list.sort(jsonSort._sortAsc(property));
	},
	desc: function(list, property){
		return list.sort(jsonSort._sortDesc(property));
	}
}

var url = {
	params: function () {
		var match,
		pl     = /\+/g,  // Regex for replacing addition symbol with a space
		search = /([^&=]+)=?([^&]*)/g,
		decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
		query  = window.location.search.substring(1);
		var urlParams = {};
		while (match = search.exec(query))
		   urlParams[decode(match[1])] = decode(match[2]);
		return urlParams;
	},

	param : function (nomeParametro) {
		var results = new RegExp('[\\?&]' + nomeParametro + '=([^&#]*)').exec(window.location.href);
		if (!results) {return '';}
		return url.decode(results[1]) || 0;
	},

	// public method for URL encoding
	encode : function (string) {
		return escape(this._utf8_encode(string));
	},

	// public method for URL decoding
	decode : function (string) {
		return this._utf8_decode(unescape(string));
	},

	// private method for UTF-8 encoding
	_utf8_encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";

		for (var n = 0; n < string.length; n++) {
			var c = string.charCodeAt(n);
			if (c < 128) {
				utftext += String.fromCharCode(c);
			} else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			} else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
		}

		return utftext;
	},

	// private method for UTF-8 decoding
	_utf8_decode : function (utftext) {
		var string = "";
		var i = 0;
		var c = 0, c1 = 0, c2 = 0;

		while ( i < utftext.length ) {
			c = utftext.charCodeAt(i);
			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			} else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			} else {
				c2 = utftext.charCodeAt(i+1);
				var c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
		}
		return string;
	}
}


///////////////////////////////////////////////////////////////////////////////
// Logger.js
// =========
// singleton log class (module pattern)
//
// This class creates a drawable tab at the bottom of web browser. You can slide
// up/down by clicking the tab. You can toggle on/off programmatically by
// calling Logger.toggle(), or Logger.show() and Logger.hide() respectively.
// It can be completely hidden by calling Logger.hide(). To get it back, call
// Logger.show(). It can be also enabled/disabled by Logger.enable()/disable().
// When it is disabled, a message with log() won't be written to logger.
// When you need to purge all previous messages, use Logger.clear().
//
// Use log() utility function to print a message to the log window.
// e.g.: log("Hello") : print Hello
//       log(123)     : print 123
//       log()        : print a blank line
//
// History:
//          1.19: Replaced var to let keyword.
//          1.18: Call toString() for "Object" type.
//          1.17: Print array elements for "Array" object.
//                Print "object" for Object, and "function" for Function type.
//          1.16: Removed vendor-specific border radius CSS.
//                Added left text-align on the logger container.
//          1.15: Fixed height issue for box-sizing CSS.
//          1.14: Added toggle() to draw logger window up/down.
//                Added open()/close() to slide up/down.
//                Changed show()/hide() for visibility.
//                Changed tab size smaller.
//          1.13: Added the class name along with version.
//          1.12: Removed width and added margin-left for msgDiv.
//          1.11: Added clear() function.
//          1.10: Added enable()/disable() functions.
//          1.09: Added z-index attribute on the logger container.
//          1.08: Use requestAnimationFrame() for animations.
//          1.07: Wrap a message if it is longer than the window width.
//          1.06: Print "undefined" or "null" if msg is undefined or null value.
//          1.05: Added time stamp for log() (with no param).
//          1.04: Modified handling undefined type of msg.
//          1.03: Fixed the error when msg is undefined.
//          1.02: Added sliding animation easy in/out using cosine.
//          1.01: Changed "display:none" to visibility:hidden for logDiv.
//                Supported IE v8 without transparent background.
//          1.00: First public release.
//
//  AUTHOR: Song Ho Ahn (song.ahn@gmail.com)
// CREATED: 2011-02-15
// UPDATED: 2020-05-20
//
// Copyright 2011. Song Ho Ahn
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// utility function to print message with timestamp to log
// e.g.: log("Hello")   : print Hello
//       log(123)       : print 123
//       log()          : print a blank line
function log(msg)
{
    if(arguments.length == 0)
        Logger.print(""); // print a blank line
    else
        Logger.print(msg);
};

///////////////////////////////////////////////////////////////////////////////
let Logger = (function()
{
    "use strict";

    ///////////////////////////////////////////////////////////////////////////
    // private members
    ///////////////////////////////////////////////////////////////////////////
    let version = "1.19";
    let containerDiv = null;
    let tabDiv = null;
    let logDiv = null;
    let visible = true;     // flag for visibility
    let opened = false;     // flag for toggle on/off
    let enabled = true;     // does not accept log messages any more if it is false
    let logHeight = 200;    // 204 + 2*padding + border-top
    let tabHeight = 35;
    // for animation
    let animTime = 0;
    let animDuration = 200; // ms
    let animFrameTime= 16;  // ms

    ///////////////////////////////////////////////////////////////////////////
    // get time and date as string with a trailing space
    let getTime = function()
    {
        let now = new Date();
        let hour = "0" + now.getHours();
        hour = hour.substring(hour.length-2);
        let minute = "0" + now.getMinutes();
        minute = minute.substring(minute.length-2);
        let second = "0" + now.getSeconds();
        second = second.substring(second.length-2);
        return hour + ":" + minute + ":" + second;
    };
    let getDate = function()
    {
        let now = new Date();
        let year = "" + now.getFullYear();
        let month = "0" + (now.getMonth()+1);
        month = month.substring(month.length-2);
        let date = "0" + now.getDate();
        date = date.substring(date.length-2);
        return year + "-" + month + "-" + date;
    };
    ///////////////////////////////////////////////////////////////////////////
    // return available requestAnimationFrame(), otherwise, fallback to setTimeOut
    let getRequestAnimationFrameFunction = function()
    {
        let requestAnimationFrame = window.requestAnimationFrame ||
                                    window.mozRequestAnimationFrame ||
                                    window.msRequestAnimationFrame ||
                                    window.oRequestAnimationFrame ||
                                    window.webkitRequestAnimationFrame;
        if(requestAnimationFrame)
            return function(callback){ return requestAnimationFrame(callback); };
        else
            return function(callback){ return setTimeout(callback, 16); };
    };



    ///////////////////////////////////////////////////////////////////////////
    // public members
    ///////////////////////////////////////////////////////////////////////////
    let self =
    {
        ///////////////////////////////////////////////////////////////////////
        // create a div for log and attach it to document
        init: function()
        {
            // avoid redundant call
            if(containerDiv)
                return true;

            // check if DOM is ready
            if(!document || !document.createElement || !document.body || !document.body.appendChild)
                return false;

            // constants
            let CONTAINER_DIV = "loggerContainer";
            let TAB_DIV = "loggerTab";
            let LOG_DIV = "logger";
            let Z_INDEX = 9999;

            // create logger DOM element
            containerDiv = document.getElementById(CONTAINER_DIV);
            if(!containerDiv)
            {
                // container
                containerDiv = document.createElement("div");
                containerDiv.id = CONTAINER_DIV;
                containerDiv.setAttribute("style", "width:100%;" +
                                                   "margin:0;" +
                                                   "padding:0;" +
                                                   "text-align:left;" +
                                                   "box-sizing:border-box;" +
                                                   "position:fixed;" +
                                                   "left:0;" +
                                                   "z-index:" + Z_INDEX + ";" +
                                                   "bottom:" + (-logHeight) + "px;");  /* hide it initially */

                // tab
                tabDiv = document.createElement("div");
                tabDiv.id = TAB_DIV;
                tabDiv.appendChild(document.createTextNode("- Fx -"));
                tabDiv.setAttribute("style", "width:70px;" +
                                             "box-sizing:border-box;" +
                                             "overflow:hidden;" +
                                             "font:bold 10px verdana,helvetica,sans-serif;" +
                                             "line-height:" + (tabHeight-1) + "px;" +  /* subtract top-border */
                                             "color:#fff;" +
                                             "position:absolute;" +
                                             "left:0px;" +
                                             "top:" + -tabHeight + "px;" +
                                             "margin:0; padding:0;" +
                                             "text-align:center;" +
                                             "border:1px solid #aaa;" +
                                             "border-bottom:none;" +
                                             /*"background:#333;" + */
                                             "background:darkblue;" +
                                             "border-top-right-radius:8px;" +
                                             "border-top-left-radius:8px;");
                // add mouse event handlers
                tabDiv.onmouseover = function()
                {
                    this.style.cursor = "pointer";
                    this.style.textShadow = "0 0 1px #fff, 0 0 2px #0f0, 0 0 6px #0f0";
                };
                tabDiv.onmouseout = function()
                {
                    this.style.cursor = "auto";
                    this.style.textShadow = "none";
                };
                tabDiv.onclick = function()
                {
                    Logger.toggle();
                    this.style.textShadow = "none";
                };

                // log message
                logDiv = document.createElement("div");
                logDiv.id = LOG_DIV;
                logDiv.setAttribute("style", "font:12px monospace;" +
                                             "height: " + logHeight + "px;" +
                                             "box-sizing:border-box;" +
                                             "color:#fff;" +
                                             "overflow-x:hidden;" +
                                             "overflow-y:scroll;" +
                                             "visibility:hidden;" +
                                             "position:relative;" +
                                             "bottom:0px;" +
                                             "margin:0px;" +
                                             "padding:5px;" +
                                             "padding-bottom:50px;" +
                                             /*"background:#333;" + */
                                             "background:rgba(0,0,0,0.8);" +
                                             "border-top:1px solid #aaa;");

                // style for log message
                let span = document.createElement("span");  // for coloring text
                span.style.color = "#afa";
                span.style.fontWeight = "bold";

                // the first message in log
                let msg = "===== Log Started at " + getDate() + ", " + getTime() + " =====";

                //span.appendChild(document.createTextNode(msg));
                //logDiv.appendChild(span);
                //logDiv.appendChild(document.createElement("br"));   // blank line
                logDiv.appendChild(document.createElement("br"));   // blank line

                // add divs to document
                containerDiv.appendChild(tabDiv);
                containerDiv.appendChild(logDiv);
                document.body.appendChild(containerDiv);
            }

            return true;
        },
        ///////////////////////////////////////////////////////////////////////
        // print log message to logDiv
        print: function(msg)
        {
            // ignore message if it is disabled
            if(!enabled)
                return;

            // check if this object is initialized
            if(!containerDiv)
            {
                let ready = this.init();
                if(!ready)
                    return;
            }

            let msgDefined = true;

            // convert non-string type to string
            if(typeof msg == "undefined")       // print "undefined" if param is not defined
            {
                msg = "undefined";
                msgDefined = false;
            }
            else if(typeof msg == "function")   // print "function" if param is function ptr
            {
                msg = "function";
                msgDefined = false;
            }
            else if(msg === null)               // print "null" if param has null value
            {
                msg = "null";
                msgDefined = false;
            }
            else
            {
                if(msg instanceof Array)        // print array elements if param is array object
                {
                    msg = this.arrayToString(msg);
                }
                else if(msg instanceof Object)  // invoke toString() if param is object type
                {
                    msg = msg.toString();
                }
                else
                {
                    msg += ""; // for other types
                }
            }

            let lines = msg.split(/\r\n|\r|\n/);
            for(let i = 0, c = lines.length; i < c; ++i)
            {
                // format time and put the text node to inline element
                let timeDiv = document.createElement("div");            // color for time
                timeDiv.setAttribute("style", "color:#999;" +
                                              "float:left;");

                let timeNode = document.createTextNode(getTime() + "\u00a0");
                //timeDiv.appendChild(timeNode);

                // create message span
                let msgDiv = document.createElement("div");
                msgDiv.setAttribute("style", "word-wrap:break-word;" +  // wrap msg
                                             "margin-left:1.0em;");     // margin-left = 9 * ?
                if(!msgDefined)
                    msgDiv.style.color = "#afa"; // override color if msg is not defined

                // put message into a text node
                let line = lines[i].replace(/ /g, "\u00a0");
                let msgNode = document.createTextNode(line);
                msgDiv.appendChild(msgNode);

                // new line div with clearing css float property
                let newLineDiv = document.createElement("div");
                newLineDiv.setAttribute("style", "clear:both;");

                logDiv.appendChild(timeDiv);            // add time
                logDiv.appendChild(msgDiv);             // add message
                logDiv.appendChild(newLineDiv);         // add message

                logDiv.scrollTop = logDiv.scrollHeight; // scroll to last line
            }
        },
        ///////////////////////////////////////////////////////////////////////
        // slide log container up and down
        toggle: function()
        {
            if(opened)  // if opened, close the window
                this.close();
            else        // if closed, open the window
                this.open();
        },
        open: function()
        {
            if(!this.init()) return;
            if(!visible) return;
            if(opened) return;

            logDiv.style.visibility = "visible";
            animTime = Date.now();
            let requestAnimationFrame = getRequestAnimationFrameFunction();
            requestAnimationFrame(slideUp);
            function slideUp()
            {
                let duration = Date.now() - animTime;
                if(duration >= animDuration)
                {
                    containerDiv.style.bottom = 0;
                    opened = true;
                    return;
                }
                let y = Math.round(-logHeight * (1 - 0.5 * (1 - Math.cos(Math.PI * duration / animDuration))));
                containerDiv.style.bottom = "" + y + "px";
                requestAnimationFrame(slideUp);
            }
        },
        close: function()
        {
            if(!this.init()) return;
            if(!visible) return;
            if(!opened) return;

            animTime = Date.now();
            let requestAnimationFrame = getRequestAnimationFrameFunction();
            requestAnimationFrame(slideDown);
            function slideDown()
            {
                let duration = Date.now() - animTime;
                if(duration >= animDuration)
                {
                    containerDiv.style.bottom = "" + -logHeight + "px";
                    logDiv.style.visibility = "hidden";
                    opened = false;
                    return;
                }
                let y = Math.round(-logHeight * 0.5 * (1 - Math.cos(Math.PI * duration / animDuration)));
                containerDiv.style.bottom = "" + y + "px";
                requestAnimationFrame(slideDown);
            }
        },
        ///////////////////////////////////////////////////////////////////////
        // show/hide the logger window and tab
        show: function()
        {
            if(!this.init())
                return;

            containerDiv.style.display = "block";
            visible = true;
        },
        hide: function()
        {
            if(!this.init())
                return;

            containerDiv.style.display = "none";
            visible = false;
        },
        ///////////////////////////////////////////////////////////////////////
        // when Logger is enabled (default), log() method will write its message
        // to the console ("logDiv")
        enable: function()
        {
            if(!this.init())
                return;

            enabled = true;
            tabDiv.style.color = "#fff";
            logDiv.style.color = "#fff";
        },
        ///////////////////////////////////////////////////////////////////////
        // when it is diabled, subsequent log() calls will be ignored and
        // the message won't be written on "logDiv".
        // "LOG" tab and log text are grayed out to indicate it is disabled.
        disable: function()
        {
            if(!this.init())
                return;

            enabled = false;
            tabDiv.style.color = "#666";
            logDiv.style.color = "#666";
        },
        ///////////////////////////////////////////////////////////////////////
        // clear all messages from logDiv
        clear: function()
        {
            if(!this.init())
                return;

            logDiv.innerHTML = "";
        },
        ///////////////////////////////////////////////////////////////////////
        // utility funtions
        arrayToString: function(array)
        {
            let str = "[";
            for(let i = 0, c = array.length; i < c; ++i)
            {
                if(array[i] instanceof Array)
                    str += this.arrayToString(array[i]);
                else
                    str += array[i];

                if(i < c - 1)
                    str += ", ";
            }
            str += "]";
            return str;
        }
    };
    return self;
})();